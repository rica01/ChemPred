#! /bin/python
import sys
import tornado.ioloop
import tornado.web
from src.webserver.StartProcedure import StartProcedure
from src.webserver.Zero import Zero
from src.webserver.One import One
from src.webserver.Two import Two
from src.webserver.Three import Three
from src.webserver.Four import Four

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        print("MainHandler")
        self.render("./src/webserver/html/start.html")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/procedure", StartProcedure),
        (r"/zero", Zero),
        (r"/one", One),
        (r"/two", Two),
        (r"/three", Three),
        (r"/four", Four)

    ])

if __name__ == "__main__":
    print("starting server...")
    app = make_app()
    app.listen(int(sys.argv[1]))
    print("server running in port "+sys.argv[1])

    tornado.ioloop.IOLoop.current().start()
