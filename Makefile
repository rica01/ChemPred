
BASEDIR=$(shell pwd)
BINDIR=$(BASEDIR)/bin

WEKABASE_JAR=$(BASEDIR)/src/tools/weka-3-9-1/weka.jar
WEKAXMEANS_JAR=$(BASEDIR)/src/tools/weka-3-9-1/packages/XMeans/XMeans.jar

XMEANS_SRC=$(BASEDIR)/src/methods/xmeans
HIERARCHICALCLUSTERER_SRC=$(BASEDIR)/src/methods/hierarchicalclusterer
EXPECTATIONMAXIMIZATION_SRC=$(BASEDIR)/src/methods/expectationmaximization

ONER_SRC=$(BASEDIR)/src/methods/expectationmaximization
RANDOMFOREST_SRC=$(BASEDIR)/src/methods/randomforst

CLASSPATH=.:$(WEKABASE_JAR):$(WEKAXMEANS_JAR):$(XMEANS_SRC):$(RANDOMFOREST_SRC):$(BINDIR)

JAVAPATH=/usr/bin



build:  build-xmeans build-randomforest build-merge



##################################################
build-supportvectormachine:
	$(JAVAPATH)/javac -cp $(CLASSPATH) src/methods/supportvectormachine/SupportVectorMachineDemo.java
	mv src/methods/supportvectormachine/SupportVectorMachineDemo.class bin/

run-supportvectormachine:
	$(JAVAPATH)/java -cp $(CLASSPATH) MultilayerPerceptronDemo 

##################################################
##################################################
build-multilayerperceptron:
	$(JAVAPATH)/javac -cp $(CLASSPATH) src/methods/multilayerperceptron/MultilayerPerceptronDemo.java
	mv src/methods/multilayerperceptron/MultilayerPerceptronDemo.class bin/

run-multilayerperceptron:
	$(JAVAPATH)/java -cp $(CLASSPATH) MultilayerPerceptronDemo 

##################################################
##################################################
build-hierarchicalclusterer:
	$(JAVAPATH)/javac -cp $(CLASSPATH) src/methods/hierarchicalclusterer/MultilayerPerceptronDemo.java
	mv src/methods/hierarchicalclusterer/HierarchicalClustererDemo.class bin/

run-hierarchicalcluster:
	$(JAVAPATH)/java -cp $(CLASSPATH) HierarchicalClustererDemo

##################################################
##################################################
build-xmeans:
	$(JAVAPATH)/javac -cp $(CLASSPATH) src/methods/xmeans/XMeansDemo.java
	mv src/methods/xmeans/XMeansDemo.class bin/

run-xmeans:
	$(JAVAPATH)/java -cp $(CLASSPATH) XMeansDemo

##################################################
##################################################
build-expectationmaximization:
	$(JAVAPATH)/javac -cp $(CLASSPATH) src/methods/expectationmaximization/ExpectationMaximizationDemo.java
	mv src/methods/expectationmaximization/ExpectationMaximizationDemo.class bin/

run-expectationmaximization:
	$(JAVAPATH)/java -cp $(CLASSPATH) ExpectationMaximizationDemo

##################################################
##################################################
build-randomforest:
	$(JAVAPATH)/javac -cp $(CLASSPATH) src/methods/randomforest/RandomForestDemo.java
	mv src/methods/randomforest/RandomForestDemo.class bin/

run-randomforest:
	$(JAVAPATH)/java -cp $(CLASSPATH) RandomForestDemo

##################################################
##################################################
build-merge:
	g++ -std=c++0x src/tools/merge.cpp -o bin/merge.cpp.exe -lpthread

##################################################
##################################################
clean:
	rm -rf bin/merge.cpp.exe
	rm -rf bin/XMeansDemo.class
	rm -rf bin/RandomForestDemo.class
