import pandas

# cellLine, therapy, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000
dfDrug = pandas.read_csv('mini_drug.csv', header=None)

# cellLine, id, gene, gene id, CN, GE, geneLoc, tissue
dfGene = pandas.read_csv('mini_gene.csv', header=None)

# cellLine, gene, therapy, tissue, CN, GE, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000
dictOut = dict()
i=0
for d in range(0, len(dfDrug)):
    for g in range(0, len(dfGene)):
	#print(i)
        if dfDrug.loc[d,0] == dfGene.loc[g,0]:
            dictOut[i] = {'cellLine':dfDrug.loc[d, 0], 'therapy':dfDrug.loc[d, 1], 'ec50':dfDrug.loc[d, 2], 'ic50':dfDrug.loc[d, 3], 'aMax':dfDrug.loc[d, 4], 'actA':dfDrug.loc[d, 5], 'm0_0025':dfDrug.loc[d, 6], 'sd0_0025':dfDrug.loc[d, 7], 'm0_0080':dfDrug.loc[d, 8], 'sd0_0080':dfDrug.loc[d, 9], 'm0_0250':dfDrug.loc[d, 10], 'sd0_0250':dfDrug.loc[d, 11], 'm0_0800':dfDrug.loc[d, 12], 'sd0_800':dfDrug.loc[d, 13], 'm0_2500':dfDrug.loc[d, 14], 'sd0_2500':dfDrug.loc[d, 15], 'm0_8000':dfDrug.loc[d, 16], 'sd0_8000':dfDrug.loc[d, 17], 'm2_5300':dfDrug.loc[d, 18], 'sd2_5300':dfDrug.loc[d, 19], 'm8_0000':dfDrug.loc[d, 20], 'sd8_0000':dfDrug.loc[d, 21], 'gene':dfGene.loc[g, 2], 'copyNumber':dfGene.loc[g, 4], 'geneExpression':dfGene.loc[g, 5], 'geneLocation':dfGene.loc[g, 6], 'tissue':dfGene.loc[g, 7]}
            #print(dictOut[i])
            i=i+1
print(dictOut)
