#include <iostream>
#include <string>
#include <list>
#include <cstdlib>
#include "csv.h"
using namespace std;
using namespace io;

class drugRecord
{
    // cellLine, therapy, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000
    public:
    string cellLine;
    string therapy;
    double ec50;
    double ic50;
    double aMax;
    double actA;
    double m0_0025; 
    double sd0_0025;
    double m0_0080;
    double sd0_0080;
    double m0_0250;
    double sd0_0250;
    double m0_0800;
    double sd0_800;
    double m0_2500;
    double sd0_2500;
    double m0_8000;
    double sd0_8000;
    double m2_5300;
    double sd2_5300;
    double m8_0000;
    double sd8_0000;
    
    void printRecord(){
        cout << "D-------------------------------------" << endl;
        cout << "cellLine: " << this->cellLine << ", ";
        cout << "therapy: " << this->therapy << ", ";
        cout << "ec50: " << this->ec50 << ", ";
        cout << "ic50: " << this->ic50 << ", ";
        cout << "aMax: " << this->aMax << ", ";
        cout << "actA: " << this->actA << ", ";
        cout << "m0_0025: " << this->m0_0025 << ", ";
        cout << "sd0_0025: " << this->sd0_0025 << ", ";
        cout << "m0_0080: " << this->m0_0080 << ", ";
        cout << "sd0_0080: " << this->sd0_0080 << ", ";
        cout << "m0_0250: " << this->m0_0250 << ", ";
        cout << "sd0_0250: " << this->sd0_0250 << ", ";
        cout << "m0_0800: " << this->m0_0800 << ", ";
        cout << "sd0_800: " << this->sd0_800 << ", ";
        cout << "m0_2500: " << this->m0_2500 << ", ";
        cout << "sd0_2500: " << this->sd0_2500 << ", ";
        cout << "m0_8000: " << this->m0_8000 << ", ";
        cout << "sd0_8000: " << this->sd0_8000 << ", ";
        cout << "m2_5300: " << this->m2_5300 << ", ";
        cout << "sd2_5300: " << this->sd2_5300 << ", ";
        cout << "m8_0000: " << this->m8_0000 << ", ";
        cout << "sd8_0000: " << this->sd8_0000 << endl;
        
        return;
    }
};

class geneRecord
{
    // # cellLine, id, gene, gene id, CN, GE, geneLoc, tissue
    public:
    string cellLine;
    string id;
    string gene;
    string geneId;
    double copyNumber;
    double geneExpression;
    int geneLocation;
    string tissue;
    
    void printRecord(){
        cout << "G-------------------------------------" << endl;
        cout << "cellLine: " << this->cellLine << ", ";
        cout << "id: " << this->id << ", ";
        cout << "gene: " << this->gene << ", ";
        cout << "geneId: " << this->geneId << ", ";
        cout << "geneLocation: " << this->geneLocation << ", ";
        cout << "copyNumber: " << this->copyNumber << ", ";
        cout << "geneExpression: " << this->geneExpression << ", ";
        cout << "tissue: " << this->tissue << endl;
        
        return;
    }
    
};

class outRecord
{
    // cellLine, therapy, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000
    public:
    string cellLine;
    string therapy;
    string tissue;
    string gene;
    int geneLocation;
    double copyNumber;
    double geneExpression;
    double ec50;
    double ic50;
    double aMax;
    double actA;
    double m0_0025;
    double sd0_0025;
    double m0_0080;
    double sd0_0080;
    double m0_0250;
    double sd0_0250;
    double m0_0800;
    double sd0_800;
    double m0_2500;
    double sd0_2500;
    double m0_8000;
    double sd0_8000;
    double m2_5300;
    double sd2_5300;
    double m8_0000;
    double sd8_0000;

    void printRecord(){
        cout << "O-------------------------------------" << endl;
        cout << "cellLine: " << this->cellLine << ", ";
        cout << "therapy: " << this->therapy << ", ";
        cout << "tissue: " << this->tissue << ", ";
        cout << "gene: " << this->gene << ", ";
        cout << "geneLocation: " << this->geneLocation << ", ";
        cout << "copyNumber: " << this->copyNumber << ", ";
        cout << "geneExpression: " << this->geneExpression << ", ";
        cout << "ec50: " << this->ec50 << ", ";
        cout << "ic50: " << this->ic50 << ", ";
        cout << "aMax: " << this->aMax << ", ";
        cout << "actA: " << this->actA << ", ";
        cout << "m0_0025: " << this->m0_0025 << ", ";
        cout << "sd0_0025: " << this->sd0_0025 << ", ";
        cout << "m0_0080: " << this->m0_0080 << ", ";
        cout << "sd0_0080: " << this->sd0_0080 << ", ";
        cout << "m0_0250: " << this->m0_0250 << ", ";
        cout << "sd0_0250: " << this->sd0_0250 << ", ";
        cout << "m0_0800: " << this->m0_0800 << ", ";
        cout << "sd0_800: " << this->sd0_800 << ", ";
        cout << "m0_2500: " << this->m0_2500 << ", ";
        cout << "sd0_2500: " << this->sd0_2500 << ", ";
        cout << "m0_8000: " << this->m0_8000 << ", ";
        cout << "sd0_8000: " << this->sd0_8000 << ", ";
        cout << "m2_5300: " << this->m2_5300 << ", ";
        cout << "sd2_5300: " << this->sd2_5300 << ", ";
        cout << "m8_0000: " << this->m8_0000 << ", ";
        cout << "sd8_0000: " << this->sd8_0000 << endl;
        
        return;
    }
};



int main()
{
    list<geneRecord> geneRecords = list<geneRecord>();
    list<drugRecord> drugRecords = list<drugRecord>();
    list<outRecord> outRecords = list<outRecord>();
    
    
    //"SNU520_STOMACH","904","CARD14","79092","4.413715","-0.12980","18252","stomach"
    io::CSVReader<8, trim_chars<' ', '\t', '"'>> geneCSV("gene.csv");
    geneCSV.set_header("cellLine", "id", "gene", "geneId", "cn", "ge", "geneLoc", "tissue");
    {
        string cellLine, id, gene, geneId, tissue, cn, ge, geneLoc;
        while(geneCSV.read_row(cellLine, id, gene, geneId, cn, ge, geneLoc, tissue)){
            geneRecord r;
            string::size_type sz;

            r.cellLine = cellLine;
            r.id = id;
            r.gene = gene;
            r.geneId = geneId;
            r.copyNumber = atof(cn.c_str());
            r.geneExpression = atof(ge.c_str());
            r.geneLocation = atoi(geneLoc.c_str());
            r.tissue = tissue;
            //r.printRecord();
            geneRecords.push_back(r);
        }
    }
    
    
    // cellLine, therapy, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000
    io::CSVReader<22, trim_chars<' ', '\t', '"'>> drugCSV("drug.csv");
    drugCSV.set_header("cellLine", "therapy", "ec50", "ic50", "aMax", "actA", "m0_0025", "sd0_0025", "m0_0080", "sd0_0080", "m0_0250", "sd0_0250", "m0_0800", "sd0_800", "m0_2500", "sd0_2500", "m0_8000", "sd0_8000", "m2_5300", "sd2_5300", "m8_0000", "sd8_0000");
    {
        string cellLine, therapy, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000;
        while(drugCSV.read_row(cellLine, therapy, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000)){        
            drugRecord r;
            
            r.cellLine = cellLine;
            r.therapy = therapy;
            r.ec50 = atof(ec50.c_str());
            r.ic50 = atof(ic50.c_str());
            r.aMax = atof(aMax.c_str());
            r.actA = atof(actA.c_str());
            r.m0_0025 = atof(m0_0025.c_str());
            r.sd0_0025 = atof(sd0_0025.c_str());
            r.m0_0080 = atof(m0_0080.c_str());
            r.sd0_0080 = atof(sd0_0080.c_str());
            r.m0_0250 = atof(m0_0250.c_str());
            r.sd0_0250 = atof(sd0_0250.c_str());
            r.m0_0800 = atof(m0_0800.c_str());
            r.sd0_800 = atof(sd0_800.c_str());
            r.m0_2500 = atof(m0_2500.c_str());
            r.sd0_2500 = atof(sd0_2500.c_str());
            r.m0_8000 = atof(m0_8000.c_str());
            r.sd0_8000 = atof(sd0_8000.c_str());
            r.m2_5300 = atof(m2_5300.c_str());
            r.sd2_5300 = atof(sd2_5300.c_str());
            r.m8_0000 = atof(m8_0000.c_str());
            r.sd8_0000 = atof(sd8_0000.c_str());
            
            //r.printRecord();
            
            drugRecords.push_back(r);
        }
    }
    
    
    
    // cellLine, gene, therapy, tissue, CN, GE, ec50, ic50, aMax, actA, m0_0025, sd0_0025, m0_0080, sd0_0080, m0_0250, sd0_0250, m0_0800, sd0_800, m0_2500, sd0_2500, m0_8000, sd0_8000, m2_5300, sd2_5300, m8_0000, sd8_0000

    cout << endl << endl;
    for (list<drugRecord>::iterator itD=drugRecords.begin(); itD != drugRecords.end(); ++itD)
    {
        for (list<geneRecord>::iterator itG=geneRecords.begin(); itG != geneRecords.end(); ++itG)
        {
            if((*itG).cellLine == (*itD).cellLine)
            {
                outRecord r;
                
                r.cellLine = (*itD).cellLine;
                r.therapy = (*itD).therapy;
                r.ec50 = (*itD).ec50;
                r.ic50 = (*itD).ic50;
                r.aMax = (*itD).aMax;
                r.actA = (*itD).actA;
                r.m0_0025 = (*itD).m0_0025;
                r.sd0_0025 = (*itD).sd0_0025;
                r.m0_0080 = (*itD).m0_0080;
                r.sd0_0080 = (*itD).sd0_0080;
                r.m0_0250 = (*itD).m0_0250;
                r.sd0_0250 = (*itD).sd0_0250;
                r.m0_0800 = (*itD).m0_0800;
                r.sd0_800 = (*itD).sd0_800;
                r.m0_2500 = (*itD).m0_2500;
                r.sd0_2500 = (*itD).sd0_2500;
                r.m0_8000 = (*itD).m0_8000;
                r.sd0_8000 = (*itD).sd0_8000;
                r.m2_5300 = (*itD).m2_5300;
                r.sd2_5300 = (*itD).sd2_5300;
                r.m8_0000 = (*itD).m8_0000;
                r.sd8_0000 = (*itD).sd8_0000;
                
                
                r.gene = (*itG).gene;
                r.copyNumber = (*itG).copyNumber;
                r.geneExpression = (*itG).geneExpression;
                r.geneLocation = (*itG).geneLocation;
                r.tissue = (*itG).tissue;
                
                
                //(*itG).printRecord();
                //(*itD).printRecord();
                //r.printRecord();
                cout << r.cellLine << ", " << r.therapy<< ", " << r.gene<< ", " << r.copyNumber<< ", " << r.geneExpression<< ", " << r.geneLocation<< ", " << r.tissue<< ", " << r.ec50<< ", " << r.ic50<< ", " << r.aMax<< ", " << r.actA<< ", " << r.m0_0025<< ", " << r.sd0_0025<< ", " << r.m0_0080<< ", " << r.sd0_0080<< ", " << r.m0_0250<< ", " << r.sd0_0250<< ", " << r.m0_0800<< ", " << r.sd0_800<< ", " << r.m0_2500<< ", " << r.sd0_2500<< ", " << r.m0_8000<< ", " << r.sd0_8000<< ", " << r.m2_5300<< ", " << r.sd2_5300<< ", " << r.m8_0000<< ", " << r.sd8_0000<< endl;
                //outRecords.push_back(r);
            }
        }
    }
    
    
    return 0;
}
