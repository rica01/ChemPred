
import java.util.Arrays;

import java.io.BufferedReader;
import java.io.FileReader;
import weka.core.*;

import weka.core.Instances;
import weka.clusterers.DensityBasedClusterer;

import weka.classifiers.functions.SMO;

import weka.clusterers.ClusterEvaluation;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;



public class SupportVectorMachineDemo {   

    public SupportVectorMachineDemo(String filename) throws Exception {
        Instances data = new Instances(new BufferedReader(new FileReader(filename)));
        
        
        //check for class column
        //removing class column
        /*
        Remove remove = new Remove();
        remove.setAttributeIndices("5");
        remove.setInvertSelection(false);
        remove.setInputFormat(data);
        data = Filter.useFilter(data, remove);
        */


        data.setClassIndex(data.numAttributes()-1);

        SMO svm = new SMO();

        
		try {
			svm.buildClassifier(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
        }
        
        System.out.println("====== SupportVectorMachine ======");
        System.out.println("Options: " + Arrays.toString(svm.getOptions()));
        
        for(int i=0;i<data.numInstances();i++)
        {
            
            System.out.printf("%d: [%s]\t -> Cluster [%s]\n", i, data.instance(i), Arrays.toString(svm.distributionForInstance(data.instance(i))));
            
        }
        
        weka.core.SerializationHelper.write("../models/mp.model", svm);

    }

    public static void main(String[] args) throws Exception {

	System.out.println("Data file: "+args[0]);
        new SupportVectorMachineDemo(args[0]);
    }
}

