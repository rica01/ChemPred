
import java.util.Arrays;

import java.io.BufferedReader;
import java.io.FileReader;
import weka.core.*;

import weka.core.Instances;
import weka.clusterers.DensityBasedClusterer;

import weka.classifiers.functions.MultilayerPerceptron;

import weka.clusterers.ClusterEvaluation;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;



public class MultilayerPerceptronDemo {   

    public MultilayerPerceptronDemo(String filename) throws Exception {
        Instances data = new Instances(new BufferedReader(new FileReader(filename)));
        
        
        //check for class column
        //removing class column
        /*
        Remove remove = new Remove();
        remove.setAttributeIndices("5");
        remove.setInvertSelection(false);
        remove.setInputFormat(data);
        data = Filter.useFilter(data, remove);
        */


        data.setClassIndex(data.numAttributes()-1);

        MultilayerPerceptron mp = new MultilayerPerceptron();

        
		try {
			mp.buildClassifier(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
        }
        
        System.out.println("====== MultilayerPerceptron ======");
        System.out.println("Options: " + Arrays.toString(mp.getOptions()));
        
        for(int i=0;i<data.numInstances();i++)
        {
            
            System.out.printf("%d: [%s]\t -> Cluster [%s]\n", i, data.instance(i), Arrays.toString(mp.distributionForInstance(data.instance(i))));
            
        }
        
        weka.core.SerializationHelper.write("../models/mp.model", mp);

    }

    public static void main(String[] args) throws Exception {

	System.out.println("Data file: "+args[0]);
        new MultilayerPerceptronDemo(args[0]);
    }
}

