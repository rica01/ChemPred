
import java.util.Arrays;

import java.io.BufferedReader;
import java.io.FileReader;
import weka.core.*;

import weka.core.Instances;
import weka.clusterers.DensityBasedClusterer;

import weka.clusterers.EM;
import weka.clusterers.XMeans;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.OneR;

import weka.clusterers.ClusterEvaluation;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;



public class XMeansDemo {   

    public XMeansDemo(String filename) throws Exception {
        Instances data = new Instances(new BufferedReader(new FileReader(filename)));
        
        
        //check for class column
        //removing class column
        Remove remove = new Remove();
        remove.setAttributeIndices("5");
        remove.setInvertSelection(false);
        remove.setInputFormat(data);
        data = Filter.useFilter(data, remove);
  
        XMeans xm = new XMeans();
        xm.buildClusterer(data);       
        
        System.out.println("====== Xmeans ======");
        System.out.println("Options: "+Arrays.toString(xm.getOptions()));
        
        for(int i=0;i<data.numInstances();i++)
        {
            System.out.printf("%d: [%s]\t -> Cluster %d \n", i, data.instance(i) ,xm.clusterInstance(data.instance(i)));
        }
        
        weka.core.SerializationHelper.write("../models/xm.model", xm);

    }

    public static void main(String[] args) throws Exception {

	System.out.println("Data file: "+args[0]);
        new XMeansDemo(args[0]);
    }
}

