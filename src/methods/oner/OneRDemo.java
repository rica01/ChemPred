
import java.util.Arrays;

import java.io.BufferedReader;
import java.io.FileReader;
import weka.core.*;

import weka.core.Instances;
import weka.clusterers.DensityBasedClusterer;
import weka.clusterers.EM;
import weka.clusterers.XMeans;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.OneR;

import weka.clusterers.ClusterEvaluation;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;



public class OneRDemo {   

    public HierarchicalClustererDemo(String filename) throws Exception {
        Instances data = new Instances(new BufferedReader(new FileReader(filename)));
        
        
        //check for class column
        //removing class column
        Remove remove = new Remove();
        remove.setAttributeIndices("5");
        remove.setInvertSelection(false);
        remove.setInputFormat(data);
        data = Filter.useFilter(data, remove);
  
        EM em = new EM();
        em.buildClusterer(data);       
        
        System.out.println("====== HierarchicalClusterer ======");
        System.out.println("Options: " + Arrays.toString(em.getOptions()));
        
        for(int i=0;i<data.numInstances();i++)
        {
            System.out.printf("%d: [%s]\t -> Cluster %d \n", i, data.instance(i), em.clusterInstance(data.instance(i)));
        }
        
        weka.core.SerializationHelper.write("../models/em.model", em);

    }

    public static void main(String[] args) throws Exception {

	System.out.println("Data file: "+args[0]);
        new HierarchicalClustererDemo(args[0]);
    }
}

