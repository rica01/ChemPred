//package RandomForestDemo;

import java.util.Arrays;
import java.util.Random;
import java.io.BufferedReader;
import java.io.FileReader;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.classifiers.Evaluation;


public class RandomForestDemo {   

    public RandomForestDemo(String filename) throws Exception {
        Instances data = new Instances(new BufferedReader(new FileReader(filename)));
        
        data.setClassIndex(data.numAttributes() - 1); //class is last attribute
  
        RandomForest rf = new RandomForest();
  

        System.out.println("====== RandomForest ======");
        System.out.println("Options: "+Arrays.toString(rf.getOptions()));  
        
        Evaluation evaluation = new Evaluation(data);
        evaluation.crossValidateModel(rf, data, 10, new Random(1));
          
         
        System.out.println(evaluation.toSummaryString("\nResults\n======\n", true));
        System.out.println(evaluation.toClassDetailsString());
        System.out.println("Results For Class -1- ");
        System.out.println("Precision=  " + evaluation.precision(0));
        System.out.println("Recall=  " + evaluation.recall(0));
        System.out.println("F-measure=  " + evaluation.fMeasure(0));
        System.out.println("Results For Class -2- ");
        System.out.println("Precision=  " + evaluation.precision(1));
        System.out.println("Recall=  " + evaluation.recall(1));
        System.out.println("F-measure=  " + evaluation.fMeasure(1));
        
        
        weka.core.SerializationHelper.write("./models/rf.model", rf);
    }

    public static void main(String[] args) throws Exception {
//    if (args.length != 1) {
//      System.out.println("usage: " + RandomForestDemo.class.getName() + " <arff-file>");
//      System.exit(1);
//    }
//
//    new RandomForestDemo(args[0]);
//    
        new RandomForestDemo("/home/rromanb/Projects/ChemPred/iris.arff");
    }
}

