import tornado.ioloop
import tornado.web
import sys
import os
import subprocess
from datetime import datetime
from Arffer import *
import constants as C


class Three(tornado.web.RequestHandler):
    def post(self):
        print('Three POST')
        self.write('Three POST'+C.htmlNL+C.htmlNL)
        arffSingleton = Arffer.getInstance()



        tableTop = """
            <table border="1" style="border-collapse: collapse;">
            <tr>
                <th>Rank</th>    
                <th width="200">Feature</th>
                <th width="200">Use</th>
                <th width="200">Class</th>
            </tr>
            """
        tableBottom = "</table>"
        tableData = ""
        

        self.write('''<html>
                    <head> 
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
                        <title></title>
                    </head>''')
        
        self.write('''<body><h1>'''+ arffSingleton.task +''' >> ''' + os.path.basename(arffSingleton.dataFile))
        if arffSingleton.task == 'predict':
            self.write(''' | ''' + os.path.basename(arffSingleton.modelFile) + ''' >>> </h1>''')
        else:
            self.write(''' >>> </h1>''')
                         


        for i in range(0, len(arffSingleton.meta.names())):
            tableData += '<tr>'
            tableData += '<td>'+str(i)+'</td>'
            tableData += '<td>' + arffSingleton.meta.names()[i] + '</td>'
            tableData += '<td><input type="checkbox" name="features" value="' + str(i) + '"></td>'
            tableData += '<td><input type="radio" name="class" value="' +  str(i) + '"></td>'
            tableData += '</tr>'
            
        table = tableTop + tableData + tableBottom

        table_with_form = '<form action="/four" method="POST">' + table + '<input type="submit" value="next">'

        self.write(table_with_form)


        self.write('</body></html>')