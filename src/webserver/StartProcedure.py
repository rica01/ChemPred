import tornado.ioloop
import tornado.web
import sys
import os
import subprocess
import constants as C


class StartProcedure(tornado.web.RequestHandler):
    
    # jobType as string and dataSet as dict
    def sendJob(self, jobType):
        try:
            #output = subprocess.check_output('qsub -w ... '+jobType+'.pbs', shell=True)
            output = subprocess.check_output("echo sending qsub...", shell=True)
            print('output: ' + output)
        except subprocess.CalledProcessError as execution:
            print(execution.returncode, execution.output)
    
    def post(self):
        
        print("processing...")
        
        
        self.write('Task: ' + str(self.get_argument('task', default=None))+C.htmlNL+C.htmlNL)
        
        for taskType in C.taskTypes:
            if self.get_argument(taskType, default=None) == 'True':
                self.write('working on task: ' + taskType + C.htmlNL)
                self.sendJob(taskType)
        
        data = 'Data:'+C.htmlNL + str(self.request.files['data'][0]['body']).replace('\n', C.htmlNL)
            
        self.write(C.htmlNL+C.htmlNL)
        self.write(data)
