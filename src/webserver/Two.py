import tornado.ioloop
import tornado.web
import sys
import os
import subprocess
from datetime import datetime
from Arffer import *
import constants as C


class Two(tornado.web.RequestHandler):

    def post(self):
        print('Two POST')
        self.write('Two POST'+C.htmlNL+C.htmlNL)

        dataFile = self.request.files['dataFile'][0]
        dataFilename = dataFile['filename']
        dataFilepath = './data/'+ str(datetime.now()).replace(' ', '_') + '_' + dataFilename
        data = str(dataFile['body'])
        with open(dataFilepath, 'w')  as f:
            f.write(data)

        arffSingleton = Arffer.getInstance()
        arffSingleton.loadFromFile(os.path.abspath(dataFilepath))
        if arffSingleton.task == 'predict':


            modelFile = self.request.files['modelFile'][0]
            modelFilename = modelFile['filename']
            modelFilepath = './data/'+ str(datetime.now()).replace(' ', '_') + '_' + modelFilename
            model = str(modelFile['body'])
            with open(modelFilepath, 'w')  as f:
                f.write(model)
            arffSingleton.modelFile = modelFilename


        self.write('''<html>
                    <head> 
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
                        <title></title>
                    </head>
                    <body>
                        <h1>'''+ arffSingleton.task +''' >> ''' + os.path.basename(arffSingleton.dataFile))
        if arffSingleton.task == 'predict':
            self.write(''' | ''' + os.path.basename(arffSingleton.modelFile) + ''' >>> </h1>''')
        else:
            self.write(''' >>> </h1>''')
                         


        self.write(str(arffSingleton.meta.name) + C.htmlNL + C.htmlNL)
        self.write('<div style="height:66%; width:80%; overflow-y: scroll;">')
        self.write(str(arffSingleton).replace('\n', C.htmlNL))
        self.write('</div><br>')
        self.write('<form action="/three" method="POST"><input type="submit" value="next">')

        self.write('</body></html>')
