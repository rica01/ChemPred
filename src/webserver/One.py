import tornado.ioloop
import tornado.web
import sys
import os
import subprocess
from datetime import datetime
import constants as C
from Arffer import *


class One(tornado.web.RequestHandler):
    def post(self):
        print("One POST")
        self.write('One POST'+C.htmlNL+C.htmlNL)


        arffSingleton = Arffer.getInstance()
        arffSingleton.setTask(self.get_argument('task'))
        
        self.write('''<html>
                    <head> 
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
                        <title></title>
                    </head>

                    <body>
                        <h1>'''+ arffSingleton.task +''' >> <h1>
                        <h3>Select a data file in ARFF format</h3>
                        <form enctype="multipart/form-data" action="/two" method="post">
                            File: <input type="file" name="dataFile" />
                            <br/>
                    ''')
        if arffSingleton.task == 'predict':
            self.write(C.htmlNL+C.htmlNL)
            self.write('''
                        <h3>Select a model file in ARFF format</h3>
                            File: <input type="file" name="modelFile" />
                            <br/>
                    ''')
        self.write('<input type="submit" value="upload" /></form>')
        self.write('</body></html>')