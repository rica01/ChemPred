import tornado.ioloop
import tornado.web
import sys
import os
import subprocess
from datetime import datetime
from scipy.io import arff
import constants as C


class Arffer:
    # Here will be the instance stored.
    __instance = None


    @staticmethod
    def getInstance():
        """ Static access method. """
        if Arffer.__instance == None:
            Arffer()
        return Arffer.__instance 

    def __init__(self):
        """ Virtually private constructor. """
        if Arffer.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            self.task = ''
            self.data = ''
            self.meta = ''
            self.dataFile = ''
            self.modelFile = ''
            self.numRecords = 0
            self.numFeatures = 0
            self.featuresToUse = []
            self.classFeature = -1
            Arffer.__instance = self
        return

    def setTask(self, t):
        self.task = t
        return

    def setFeaturesToUse(self, f):
        self.featuresToUse = f
        return

    def setClassFeature(self, c):
        self.classFeature = c
        return

    def loadFromFile(self, path):
        self.dataFile = path
        self.data, self.meta = arff.loadarff(path)
        #print(self.meta)
        for d in self.data:
            self.numRecords+=1
        self.numFeatures=len(self.data[0])
        return

    def __str__(self):
        o = str(self.meta) + '\n'
        o += str(self.numFeatures) + ' features, '
        o += str(self.numRecords) + ' records:\n'
        for i, d in enumerate(self.data):
            o += str(i) + ': ' + str(d) + '\n'
        return o